#pragma once
#include <iostream>
#include "pomocnicze.h"

using namespace std;

void dodajH3(int *tab, int liczba, int wielkoscTablicy, int n)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPelna = 0; // wartosc zwiekszana o 1 gdy indeks == start
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	while (czyPelna < 2) { // dopoki funkcja nie zatoczy kola
		if (indeks == start)
			czyPelna++;
		iloscProbek++;
		if (tab[indeks] == -1) { // sprawdzenie czy komorka o danym indeksie nie jest zajeta
			tab[indeks] = liczba; // dodanie liczby w danym miejscu
			break;
		}
		else {
			if (tab[indeks] == liczba) { // sprawdzenie czy liczba sie nie powtarza
				czyPowtorzenie++;
				break;
			}
			indeks = (indeks + funkcjaHaszujaca2(liczba, n)) % wielkoscTablicy; // przejscie do kolejnego indeksu
			//cout << indeks;
		}
	}

	if (czyPowtorzenie == 1)
		cout << "Liczba " << liczba << " juz wystepuje w tablicy H3!\n";
	else if (czyPelna == 2)
		cout << "Tablica jest pelna! Liczba " << liczba << " nie zostala dodana do tablicy H3\n";
	else
		cout << "Ilosc probek potrzebnych do dodania liczby " << liczba << " do tablicy H3 to: " << iloscProbek << endl;
}

void wyszukajH3(int *tab, int liczba, int wielkoscTablicy, int n)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPelna = 0; // wartosc zwiekszana o 1 gdy indeks == start
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	while (czyPelna < 2) { // dopoki funkcja nie zatoczy kola
		if (indeks == start)
			czyPelna++;
		iloscProbek++;
		if (tab[indeks] == liczba) { // sprawdzenie czy w komorce o danym adresie jest szukana liczba
			cout << "Liczba " << liczba << " znajduje sie na indeksie " << indeks << endl;
			break;
		}
		else
			indeks = (indeks + funkcjaHaszujaca2(liczba, n)) % wielkoscTablicy; // przejscie do kolejnego indeksu
	}

	if (czyPelna == 2)
		cout << "Nie ma takiej liczby! Liczba " << liczba << " nie zostala znaleziona w tablicy H3\n";
	else
		cout << "Ilosc probek potrzebnych do znalezienia liczby " << liczba << " w tablicy H3 to: " << iloscProbek << endl;
}

void usunH3(int *tab, int liczba, int wielkoscTablicy, int n)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPelna = 0; // wartosc zwiekszana o 1 gdy indeks == start
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	while (czyPelna < 2) { // dopoki funkcja nie zatoczy kola
		if (indeks == start)
			czyPelna++;
		iloscProbek++;
		if (tab[indeks] == liczba) { // sprawdzenie czy w komorce o danym adresie jest szukana liczba
			tab[indeks] = -1; // "usuwanie" liczby
			cout << "Liczba " << liczba << " zostala usunieta z tablicy H3\n";
			break;
		}
		else
			indeks = (indeks + funkcjaHaszujaca2(liczba, n)) % wielkoscTablicy; // przejscie do kolejnego indeksu
	}

	if (czyPelna == 2)
		cout << "Nie ma takiej liczby! Liczba " << liczba << " nie zostala znaleziona w tablicy H3\n";
}
