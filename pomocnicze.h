#pragma once
#include <iostream>

using namespace std;

void wyswietl(int *tab, int wielkoscTablicy)
{
	for (int i = 0; i < wielkoscTablicy; i++) {
		if (tab[i] != -1)
			cout << tab[i] << " ";
		else
			cout << "_ ";
	}
	cout << endl << endl;
}

// funkcja sprawdzajaca czy podana liczba jest liczba pierwsza
bool czyPierwsza(int liczba)
{
	if (liczba < 2) // gdy liczba jest mniejsza niz 2 to nie jest liczba pierwsza
		return false;

	for (int i = 2; i*i <= liczba; i++) { // sprawdzane sa liczby z zakresu od 2 do pierwiastek z danej liczby
		if (liczba % i == 0)
			return false; // zwraca false gdy reszta z dzielenia jest rowna 0
	}
	return true; // zwraca true gdy nie znaleziono zadnego dzielnika
}


int funkcjaHaszujaca(int liczba, int wielkoscTablicy)
{
	int indeks = liczba % wielkoscTablicy;
	return indeks; // zwraca indeks przeksztalcony przez funkcje haszujaca
}

int funkcjaHaszujaca2(int liczba, int n)
{
	int indeks = n - liczba % n; // nie moze przyjmowac wartosci rownych 0
	return indeks; // zwraca indeks przeksztalcony przez druga funkcje haszujaca
}
