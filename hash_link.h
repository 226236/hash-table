#pragma once
#include <iostream>
#include "pomocnicze.h"
#include <vector>

using namespace std;

void dodajH1(vector<int> *tab, int liczba, int wielkoscTablicy)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	for (int i = 0; i < tab[indeks].size(); i++) {
		iloscProbek++; // zwiekszana o 1 przy kazdym sprawdzaniu elementu
		if (tab[indeks][i] == liczba) {
			czyPowtorzenie++;
			break;
		}
	}
	if (czyPowtorzenie == 1)
		cout << "Liczba " << liczba << " juz wystepuje w tablicy H1!\n";
	else {
		tab[indeks].push_back(liczba); // dodanie elementu na koniec tablicy

		iloscProbek++; // zwiekszana o 1 przy dodawaniu na koniec tablicy
		cout << "Ilosc probek potrzebnych do dodania liczby " << liczba << " do tablicy H1 to: " << iloscProbek << endl;
	}
}

void wyszukajH1(vector<int> *tab, int liczba, int wielkoscTablicy)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	int czyZnaleziona = 0; // wartosc zwiekszana o 1 gdy liczba zostanie odnaleziona
	for (int i = 0; i < tab[indeks].size(); i++) {
		iloscProbek++;
		if (tab[indeks][i] == liczba) {
			cout << "Liczba " << liczba << " znajduje sie na indeksie " << indeks << " , " << i << endl;
			czyZnaleziona++;
			break;
		}
	}
	if (czyZnaleziona == 0)
		cout << "Nie ma takiej liczby! Liczba " << liczba << " nie zostala znaleziona w tablicy H1\n";
	else
		cout << "Ilosc probek potrzebnych do znalezienia liczby " << liczba << " w tablicy H1 to: " << iloscProbek << endl;
}

void usunH1(vector<int> *tab, int liczba, int wielkoscTablicy)
{
	int iloscProbek = 0; // ilosc probek potrzebnych do dodania elementu do tablicy
	int start = funkcjaHaszujaca(liczba, wielkoscTablicy); // poczatkowy indeks
	int indeks = start;
	int czyPowtorzenie = 0; // wartosc zwiekszana o 1 gdy element do dodania juz wystepuje w tablicy
	int czyZnaleziona = 0; // wartosc zwiekszana o 1 gdy liczba zostanie odnaleziona
	for (int i = 0; i < tab[indeks].size(); i++) {
		iloscProbek++;
		if (tab[indeks][i] == liczba) {
			tab[indeks].erase(tab[indeks].begin() + i);
			cout << "Liczba " << liczba << " zostala usunieta z tablicy H1\n";
			czyZnaleziona++; // liczba zostala znaleziona
			break;
		}
	}
	if (czyZnaleziona == 0)
		cout << "Nie ma takiej liczby! Liczba " << liczba << " nie zostala znaleziona w tablicy H1\n";
}

// funkcja do wyswietlania tablicy z linkowaniem
void wyswietlH1(vector<int> *tab, int wielkoscTablicy)
{
	for (int i = 0; i < wielkoscTablicy; i++) {
		for (int j = 0; j < tab[i].size(); j++) // wyswietlanie elementow z kolejnych tablic
			cout << tab[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}
