#include <iostream>
#include <cstdlib>
#include <vector>
#include <ctime>
#include "hash_link.h"
#include "hash_liniowe.h"
#include "hash_double.h"

using namespace std;

int main()
{
    srand(time(NULL));
	int wielkoscTablicy; // wielkosc tablicy haszujacej
	int liczba; // liczba wstawiana do tablicy

	while (1) { // petla trwa poki nie zostanie podana odpowiednia liczba
		cout << "Podaj wielkosc tablicy, ktora jest liczba pierwsza: ";
		cin >> wielkoscTablicy;
		if (czyPierwsza(wielkoscTablicy) == 1) {
			cout << "Podano poprawny rozmiar\n";
			break; // gdy uzytkownik podal poprawny rozmiar tablicy
		}
		else
			cout << "Wprowadzono liczbe ktora nie jest pierwsza!\n";
	}

	int n = wielkoscTablicy/2; // liczba pierwsza uzywana w drugiej funkcji haszujacej
	while (1) {
		if (czyPierwsza(n) == 1)
			break; // koniec gdy znaleziono liczbe pierwsza
		else
			n++; // gdy liczba nie jest pierwsza to sprawdzana jest nastepna
	}
	//cout << n << endl;

	// alokacja pamieci
	vector<int> *tablicaH1 = new vector<int>[wielkoscTablicy]; // tablica haszujaca z linkowaniem
	int *tablicaH2 = new int[wielkoscTablicy]; // tablica haszujaca z probkowaniem liniowym
	int *tablicaH3 = new int[wielkoscTablicy]; // tablica haszujaca z podwojnym haszowaniem

	// wypelnienie tablic -1, -1 oznacza "pusta" komorke
	for (int i = 0; i < wielkoscTablicy; i++) {
		//tablicaH1[i] = nullptr;
		tablicaH2[i] = -1;
		tablicaH3[i] = -1;
	}

	// wypelnienie tablic losowymi liczbami
	for (int i = 0; i < 16; i++) {
		liczba = rand() % 150;
		dodajH1(tablicaH1, liczba, wielkoscTablicy);
		dodajH2(tablicaH2, liczba, wielkoscTablicy);
		dodajH3(tablicaH3, liczba, wielkoscTablicy, n);
		cout << endl;
	}

	// wyswietlenie tablic
	wyswietlH1(tablicaH1, wielkoscTablicy);
	wyswietl(tablicaH2, wielkoscTablicy);
	wyswietl(tablicaH3, wielkoscTablicy);

	cout << endl;
	// wyszukiwanie elementow

	wyszukajH1(tablicaH1, 22, wielkoscTablicy);
	wyszukajH2(tablicaH2, 22, wielkoscTablicy);
	wyszukajH3(tablicaH3, 22, wielkoscTablicy, n);

	cout << endl;
	//usuwanie elementow
	usunH1(tablicaH1, 5, wielkoscTablicy);
	usunH2(tablicaH2, 5, wielkoscTablicy);
	usunH3(tablicaH3, 5, wielkoscTablicy, n);

	cout << endl;
	// wyswietlenie tablic
	wyswietlH1(tablicaH1, wielkoscTablicy);
	wyswietl(tablicaH2, wielkoscTablicy);
	wyswietl(tablicaH3, wielkoscTablicy);

	// zwolnienie pamieci
	delete[] tablicaH1;
	delete[] tablicaH2;
	delete[] tablicaH3;
}
